const mongoose = require('mongoose');
const Schema = require('mongoose').Schema;

const ArticleCommentSchema = mongoose.Schema({
    userId: {
        type: Schema.Types.ObjectId,
        index: true
    },
    postId: {
        type: Schema.Types.ObjectId,
        index: true
    },
    replyId:{
        type: Schema.Types.ObjectId,
        index: true
    },
    postTitle:{
        type: String
    },
    type:{
        type: String,
        enum: ['comment', 'reply']
    },
    userFullName:{
        type: String
    },
    userAvatar:{
        type: String
    },
    content: {
        type: String
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('ArticleComment', ArticleCommentSchema);