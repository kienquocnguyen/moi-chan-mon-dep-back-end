const ArticleComment = require('../models/article_comment.model.js');
const Article = require('../../articles/models/articles.model.js');
var ObjectId = require('mongodb').ObjectId; 
const Joi = require('@hapi/joi')
.extend(require('@hapi/joi-date'));

const schema = Joi.object().keys({
    userId: Joi.string().optional().messages({
        'string.base': `"userId" must be a 'string'`,
        'string.empty': `"userId" cannot be empty`
    }),
    postId: Joi.string().required().messages({
        'string.base': `"postId" must be a 'string'`,
        'string.empty': `"postId" cannot be empty`,
        'any.required': `Please enter your "postId"`
    }),
    replyId: Joi.string().optional().messages({
        'string.base': `"replyId" must be a 'string'`,
        'string.empty': `"replyId" cannot be empty`
    }),
    postTitle: Joi.string().required().messages({
        'string.base': `"postTitle" must be a 'string'`,
        'string.empty': `"postTitle" cannot be empty`,
        'any.required': `Please enter your "postTitle"`
    }),
    type: Joi.string().required().messages({
        'string.base': `"type" must be a 'string'`,
        'string.empty': `"type" must be 'comment' or 'reply'`,
        'any.required': `Please enter your "type"`
    }),
    userFullName: Joi.string().optional().messages({
        'string.base': `"userFullName" need to be upload`,
        'string.empty': `"userFullName" need to be upload`
    }),
    userAvatar: Joi.string().optional().messages({
        'string.base': `"userAvatar" need to be upload`,
        'string.empty': `"userAvatar" need to be upload`
    }),
    content: Joi.string().required().messages({
        'string.base': `"content" must be a 'string'`,
        'string.empty': `"content" cannot be empty`,
        'any.required': `Please enter your "content"`
    })
}).unknown();

const updateschema = Joi.object().keys({
    userId: Joi.string().optional().messages({
        'string.base': `"userId" must be a 'string'`,
        'string.empty': `"userId" cannot be empty`
    }),
    postId: Joi.string().optional().messages({
        'string.base': `"postId" must be a 'string'`,
        'string.empty': `"postId" cannot be empty`,
        'any.required': `Please enter your "postId"`
    }),
    replyId: Joi.string().optional().messages({
        'string.base': `"replyId" must be a 'string'`,
        'string.empty': `"replyId" cannot be empty`
    }),
    postTitle: Joi.string().optional().messages({
        'string.base': `"postTitle" must be a 'string'`,
        'string.empty': `"postTitle" cannot be empty`,
        'any.required': `Please enter your "postTitle"`
    }),
    type: Joi.string().optional().messages({
        'string.base': `"type" must be a 'string'`,
        'string.empty': `"type" must be 'comment' or 'reply'`,
        'any.required': `Please enter your "type"`
    }),
    userFullName: Joi.string().optional().messages({
        'string.base': `"userFullName" need to be upload`,
        'string.empty': `"userFullName" need to be upload`
    }),
    userAvatar: Joi.string().optional().messages({
        'string.base': `"userAvatar" need to be upload`,
        'string.empty': `"userAvatar" need to be upload`
    }),
    content: Joi.string().optional().messages({
        'string.base': `"content" must be a 'string'`,
        'string.empty': `"content" cannot be empty`,
        'any.required': `Please enter your "content"`
    })
}).unknown();


exports.create = async (req, res, next) =>{
    try {
        const validate = schema.validate(req.body);
        if (validate.error) {
            return next(res.status(400).send(validate.error));
        }

        if(req.user == null){
            return res.status(401).send({
                message: "Your login sessions is out of date"
            });
        }else{
            const data = req.body;
            data.userFullName = req.user.firstName + " " +  req.user.lastName;
            data.userId = req.user._id;
            data.userAvatar = req.user.avatar;
            const ariticleComment = new ArticleComment(data);
            await ariticleComment.save();
            const totalComments = await ArticleComment.find({postId: data.postId}).countDocuments();
            await Article.update({_id: data.postId}, {$set: {"totalComment": totalComments}})
            res.status(200).send(ariticleComment)
        }
    }catch(err){
        throw err;
    }
}
exports.findAllCommentsByPost = async (req, res, next) =>{
    try {
        let page = parseInt(req.query.page);
        const limit = parseInt(req.query.limit);
        if(page <= 1){
            page = 0;
        }else{
            page = parseInt(req.query.page) - 1;
        }
        const ariticleComment = await ArticleComment.find({postId:  req.query.postId}).skip(page * limit).limit(limit).sort({ createdAt: -1 })
        if(ariticleComment.length > 0){
            ArticleComment.find({postId:  req.query.postId}).countDocuments()
            .then((totalComments) =>{
                const total = {
                    "page": page + 1,
                    "limit": limit,
                    "total": totalComments
                };
                const data = {
                    "comments": ariticleComment,
                    "total": total
                }
                return res.send(data)
            })
        }else{
            return res.status(404).send({
             message: "Sorry, currently we don't have any events match with your searching."
            })
        }
    }catch(err){
        throw err;
    }
}

exports.updateComment = async (req ,res ,next) =>{
    const validate = updateschema.validate(req.body);
    if (validate.error) {
        return next(res.status(400).send(validate.error));
    }

    if(req.user == null){
        return res.status(405).send({
            message: "Bạn cần đăng nhập để thực hiện tính năng này."
        })
    }

    const checkId = await ArticleComment.findById(req.params.commentId);
    if(checkId){
        if(req.user._id.toString() !== checkId.userId.toString()){
            return res.status(401).send({
                message: "Bạn chỉ có thể chỉnh sửa bình luận của chính bạn."
            });
        }
        const updateComment = await ArticleComment.findByIdAndUpdate(req.params.commentId, req.body, {new: true} );
        if(!updateComment) {
            return res.status(404).send({
                message: "articleComment not found with id " + req.params.commentId
            });
        }else{
            res.send({message: "articleComment deleted successfully!"});
        }
    }else{
        return res.status(404).send({
            message: "articleComment not found with id " + req.params.commentId
        });
    }
}

exports.delete = async (req, res) => {
    if(req.user == null){
        return res.status(404).send({
            message: "You need a account to make comments."
        })
    }
    const checkId = await ArticleComment.findById(req.params.commentId);
    if(checkId){
        if(req.user._id.toString() !== checkId.userId.toString()){
            return res.status(401).send({
                message: "Bạn chỉ có thể xóa bình luận của chính bạn."
            });
        }
        const removeComment = await ArticleComment.findByIdAndRemove(req.params.commentId);
        if(!removeComment) {
            return res.status(404).send({
                message: "articleComment not found with id " + req.params.commentId
            });
        }else{
            res.send({message: "articleComment deleted successfully!"});
        }
    }else{
        return res.status(404).send({
            message: "articleComment not found with id " + req.params.commentId
        });
    }
};
