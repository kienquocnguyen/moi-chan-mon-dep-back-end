module.exports = router => {
    const ariticleComment = require('../controllers/article_comment.controller.js');

    
    router.post('/ariticleComment', Authorization, ariticleComment.create);
    router.put('/ariticleComment/update/:commentId', Authorization, ariticleComment.updateComment);
    router.get('/article/ariticleComment', ariticleComment.findAllCommentsByPost);
    router.delete('/ariticleComment/delete/:commentId', Authorization, ariticleComment.delete);
}