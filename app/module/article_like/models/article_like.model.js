const mongoose = require('mongoose');
const Schema = require('mongoose').Schema;

const ArticleLikeSchema = mongoose.Schema({
    userId: {
        type: Schema.Types.ObjectId,
        index: true
    },
    articleId: {
        type: Schema.Types.ObjectId,
        index: true
    },
    userFullName:{
        type: String
    },
    userAvatar:{
        type: String
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('ArticleLike', ArticleLikeSchema);