module.exports = router => {
    const articleLike = require('../controllers/article_like.controller.js');

    router.post('/articleLike', Authorization, articleLike.create);
    router.delete('/articleLike/delete', Authorization, articleLike.deleteLike);
    router.get('/articleLike/checkLiked/:articleId', Authorization , articleLike.checkUserLiked);
}