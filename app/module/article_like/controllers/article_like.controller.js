const ArticleLike = require('../models/article_like.model.js');
const Article = require('../../articles/models/articles.model.js');
const { $where } = require('../models/article_like.model.js');
const Joi = require('@hapi/joi')
.extend(require('@hapi/joi-date'));

const schema = Joi.object().keys({
    userId: Joi.string().optional().messages({
        'string.base': `"userId" must be a 'string'`,
        'string.empty': `"userId" cannot be empty`
    }),
    articleId: Joi.string().required().messages({
        'string.base': `"articleId" must be a 'string'`,
        'string.empty': `"articleId" cannot be empty`,
        'any.required': `Please enter your "articleId"`
    }),
    articleTitle: Joi.string().required().messages({
        'string.base': `"articleTitle" must be a 'string'`,
        'string.empty': `"articleTitle" cannot be empty`,
        'any.required': `Please enter your "articleTitle"`
    }),
    userFullName: Joi.string().optional().messages({
        'string.base': `"userFullName" need to be upload`,
        'string.empty': `"userFullName" need to be upload`
    }),
    userAvatar: Joi.string().optional().messages({
        'string.base': `"userAvatar" need to be upload`,
        'string.empty': `"userAvatar" need to be upload`
    })
}).unknown();

exports.create = async (req, res, next) =>{
    try {
        const validate = schema.validate(req.body);
        if (validate.error) {
            return next(res.status(400).send(validate.error));
        }
        if(req.user == null){
            return res.status(401).send({
                message: "You need to login to do this action"
            });
        }else{
            const data = req.body;
            data.userFullName = req.user.firstName + " " +  req.user.lastName;
            data.userId = req.user._id;
            data.userAvatar = req.user.avatar;
            //Check already liked
            const checkExist = await ArticleLike.find({articleId: data.articleId, userId: data.userId});
            if(checkExist.length > 0){
                res.status(403).send({
                    message: "You have already liked this post."
                })
            }else{
                const articleLike = new ArticleLike(data);
                await articleLike.save();
                const totalLike = await ArticleLike.find({articleId: data.articleId}).countDocuments();
                await Article.update({_id: data.articleId}, {$set: {"totalLike": totalLike}})
                res.status(200).send(articleLike)
            }
        }
    }catch(err){
        throw err;
    }
}

exports.checkUserLiked = async (req, res, next) =>{
    try {
        let checkLiked = false;
        if(req.user == null){
            checkLiked = false;
            return res.status(401).send({
                message: "You need to login to do this action"
            });
        }else{
            const checkExist = await ArticleLike.find({articleId: req.params.articleId, userId: req.user._id});
            if(checkExist.length > 0){
                checkLiked = true;
            }else{
                checkLiked = false;
            }
            return res.status(200).send({
                "checkLiked": checkLiked
            })
        }
    }catch(err){
        throw err;
    }
}

exports.deleteLike = async (req ,res) =>{
    if(req.user == null){
        return res.status(404).send({
            message: "You need to login to do this action."
        })
    }else{
        console.log(req.query)
        const articleLike = await ArticleLike.find({articleId: req.query.articleId, userId: req.query.userId});
        if(articleLike.length > 0){
            const removeArticleLike = await ArticleLike.findByIdAndRemove(articleLike[0]._id)
            if(removeArticleLike){
                const totalLike = await ArticleLike.find({articleId: removeArticleLike.articleId}).countDocuments();
                await Article.update({_id: articleLike[0].articleId}, {$set: {"totalLike": totalLike}})
                res.status(200).send(removeArticleLike);
            }else{
                return res.status(500).send({
                    message: "Lost internet connection"
                });
            }
        }else{
            return res.status(404).send({
                message: "You haven't like this article."
            })
        }
    }
}