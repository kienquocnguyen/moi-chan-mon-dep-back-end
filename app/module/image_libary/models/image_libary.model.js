const mongoose = require('mongoose');
const slug = require('mongoose-slug-generator');
mongoose.plugin(slug);
const Schema = require('mongoose').Schema;

const ImageLibarySchema = mongoose.Schema({
    articleId:{
        type: String
    },
    photo:{
        type: String
    },
    uploadDate:{
        type: Date
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('ImageLibary', ImageLibarySchema);