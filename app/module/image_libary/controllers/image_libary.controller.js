const ImageLibary = require('../models/image_libary.model');
const Joi = require('@hapi/joi')
.extend(require('@hapi/joi-date'));
const moment = require('moment');

const schema = Joi.object().keys({
    articleId: Joi.string().optional().messages({
        'string.base': `"articleId" must be a 'string'`,
        'string.empty': `"articleId" cannot be empty`
    }),
    photo: Joi.string().required().messages({
        'string.base': `"photo" must be a 'string'`,
        'string.empty': `"photo" cannot be empty`,
        'any.required': `Please enter your "photo"`
    }),
    uploadDate: Joi.date().optional().messages({
        'string.base': `"uploadDate" must be a date with format 'dd/mm/yy'`,
        'string.empty': `"uploadDate" cannot be empty`
    }),
}).unknown();

exports.createArticles = async (req, res, next) =>{
    try {
        const validate = schema.validate(req.body);
        if (validate.error) {
          return next(res.status(400).send(validate.error));
        }
    
        const data = req.body;
        if(req.user == null){
            return res.status(404).send({
                message: "Your login sessions is out of date"
            });
        }else{
            if(req.user.role == "admin"){
                let image = req.files.image;
                for(var i = 0; i <= image.length; i++){
                    image[i].mv('./uploads/' + image[i].name);
                    data.image[i] = image[i].name;
                }
                const images = new ImageLibary(data);
                await images.save();
                return images;
            }
            else{
                res.status(401).send({
                    message: "You must be a 'admin' to do this action"
                })
            }
        }
        
        return next();
    }catch(err){
        throw err
    }
}