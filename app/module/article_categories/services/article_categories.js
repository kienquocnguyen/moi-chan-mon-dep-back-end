const ArticleCategories = require('../models/article_categories.model.js');

exports.create = async (data) => {
    try {
            const articlesCategories = new ArticleCategories(data);
            await articlesCategories.save();
            return articlesCategories;
    }catch(e){
        throw e;
    }
}