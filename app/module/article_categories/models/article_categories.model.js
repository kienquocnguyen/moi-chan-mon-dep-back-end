const mongoose = require('mongoose');
const slug = require('mongoose-slug-generator');
const Schema = require('mongoose').Schema;
mongoose.plugin(slug);

const ArticleCategoriesSchema = mongoose.Schema({
    title: {
        type: String
    },
    description: {
        type: String
    },
    avatar:{
        type: String
    },
    parentId:{
        type: String
    },
    categoriesChildren:{
        type: Array
    },
    slug: { type: String, slug: ["title"], unique: true },
}, {
    timestamps: true
});

module.exports = mongoose.model('ArticleCategories', ArticleCategoriesSchema);