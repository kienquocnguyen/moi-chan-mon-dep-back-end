module.exports = router => {
    const ArticleCategories = require('../controllers/articles_categories.controllers.js');

    router.post('/articleCategories', Authorization, ArticleCategories.create);
    router.put('/articleCategories/update', Authorization, ArticleCategories.updateCategories);
    router.get('/articleCategories/list', ArticleCategories.findAllCategories);
    router.get('/articleCategories/fulllist', ArticleCategories.findAllCategoriesWithChildren);
    router.get('/articleCategories/:permalink', ArticleCategories.findCategoryBySlug);
    router.delete('/articleCategories/delete/:categoryId', Authorization, ArticleCategories.delete);
}