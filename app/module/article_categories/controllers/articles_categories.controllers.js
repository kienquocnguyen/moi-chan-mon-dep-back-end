const _ = require('lodash');
var ObjectId = require('mongoose').Types.ObjectId;
const ArticleCategories = require('../models/article_categories.model.js');
const ArticleCategoriesServices = require('../services/article_categories.js');
const Joi = require('@hapi/joi')
.extend(require('@hapi/joi-date'));

const schema = Joi.object().keys({
    title: Joi.string().required().messages({
        'string.base': `"title" must be a 'string'`,
        'string.empty': `"title" cannot be empty`,
        'any.required': `Please enter your "title"`
    }),
    description: Joi.string().optional().messages({
        'string.base': `"description" must be a 'string'`,
        'string.empty': `"description" cannot be empty`
    }),
    parentId: Joi.string().optional().default("").messages({
        'string.base': `"parentId" must be a 'string'`,
        'string.empty': `"parentId" cannot be empty`
    }),
    categoriesChildren: Joi.array().optional().default([]).messages({
        'array.base': `"categoriesChildren" must be an array`
    }),
    avatar: Joi.string().optional().messages({
        'string.base': `"avatar" need to be upload`,
        'string.empty': `"avatar" need to be upload`
    })
}).unknown();

exports.create = async (req, res, next) =>{
    try {
        const validate = schema.validate(req.body);
        if (validate.error) {
          return next(res.status(400).send(validate.error));
        }
    
        const data = req.body;
        if(req.user == null){
            return res.status(404).send({
                message: "Bạn đã hết phiên đăng nhập"
            });
        }else{
            if(req.user.role == "admin"){
                let avatar = req.files.avatar;
                avatar.mv('./uploads/' + avatar.name);
                data.avatar = avatar.name;
                const articlesCategories = await ArticleCategoriesServices.create(data);
                res.status(200).send({
                    message: "Bạn đã tạo danh mục thành công",
                    articlesCategories: articlesCategories
                });
            }else{
                res.status(401).send({
                    message: "Bạn cần phải có tài khoản quản trị viên để sử dụng tính năng này."
                })
            }
        }
        return next();
    }catch(err){
        throw err
    }
}

exports.updateCategories = async (req ,res ,next) =>{

    if(req.user == null){
        return res.status(405).send({
            message: "Bạn cần đăng nhập để thực hiện tính năng này."
        })
    }

    let categoryParams = JSON.stringify(req.query.categoryId);
    let categoryId = categoryParams.replace(/[~%&\\;:"',<>?#\s]/g,"");
    let parentParams = JSON.stringify(req.query.parentId);
    let parentId = parentParams.replace(/[~%&\\;:"',<>?#\s]/g,"");

    const checkId = await ArticleCategories.findById(categoryId);
    if(checkId){
        if(req.user.role == "admin"){
            if(!checkId.parentId){
                const checkParentId = await ArticleCategories.findById(parentId);
                if(checkParentId){
                    await ArticleCategories.update( {_id : categoryId } ,{$set : {parentId : parentId}});
                    await ArticleCategories.update( {_id : parentId } ,{$push : {categoriesChildren : checkId}})
                    return res.status(200).send({
                        message: "Bạn đã cập nhật danh mục thành công"
                    })
                }else{
                    return res.status(406).send({
                        message: "Danh mục cha không tồn tại"
                    })
                }
            }else{
                const checkParentId = await ArticleCategories.findById(parentId);
                if(checkParentId){
                    /*Update parentId for categories*/ await ArticleCategories.update( {_id : categoryId } ,{$set : {parentId : parentId}});
                    /*Add category to new parent category*/ await ArticleCategories.update( {_id : parentId } ,{$push : {categoriesChildren : checkId}})
                    /*Remove category from old parent category*/ await ArticleCategories.update( {_id : checkId.parentId } ,{$pull : {categoriesChildren : { _id: checkId._id }}})
                    return res.status(200).send({
                        message: "Bạn đã cập nhật danh mục thành công"
                    })
                }else{
                    return res.status(406).send({
                        message: "Danh mục cha không tồn tại"
                    })
                }
            }
        }else{
            res.status(401).send({
                message: "Bạn cần có tài khoảng quản trị viên để thực hiện hành động này"
            })
        }
    }else{
        return res.status(404).send({
            message: "ArticleCategories not found with id " + req.query.categoryId
        });
    }
}

exports.findAllCategories = async (req, res, next) =>{
    try {

        const sort = {title: -1};
        let page = parseInt(req.query.page);
        const limit = parseInt(req.query.limit);
        if(page <= 1){
            page = 0;
        }else{
            page = parseInt(req.query.page) - 1;
        }

        const articleCategories = await ArticleCategories.find().skip(page * limit).limit(limit)
        .sort(sort)
        .exec();
        if(articleCategories.length > 0){
            ArticleCategories.countDocuments()
            .then((docs) =>{
                const total = {
                    "page": page + 1,
                    "limit": limit,
                    "total": docs
                };
                const data = {
                    "articleCategories": _.filter(articleCategories, {'parentId': undefined}),
                    "total": total
                }
                return res.send(data)
            })
        }else{
            return res.status(404).send({
             message: "Bạn chưa tạo danh mục nào."
            })
        }
    }catch(err){
        throw err;
    }
}

exports.findAllCategoriesWithChildren = async (req, res, next) =>{
    try {

        const sort = {title: -1};
        let page = parseInt(req.query.page);
        const limit = parseInt(req.query.limit);
        if(page <= 1){
            page = 0;
        }else{
            page = parseInt(req.query.page) - 1;
        }

        const articleCategories = await ArticleCategories.find().skip(page * limit).limit(limit)
        .sort(sort)
        .exec();
        if(articleCategories.length > 0){
            ArticleCategories.countDocuments()
            .then((docs) =>{
                const total = {
                    "page": page + 1,
                    "limit": limit,
                    "total": docs
                };
                const data = {
                    "articleCategories": articleCategories,
                    "total": total
                }
                return res.send(data)
            })
        }else{
            return res.status(404).send({
             message: "Bạn chưa tạo danh mục nào."
            })
        }
    }catch(err){
        throw err;
    }
}

exports.delete = async (req, res) => {
    if(req.user == null){
        return res.status(405).send({
            message: "Bạn cần phải đăng nhập để sử dụng được tính năng này."
        })
    }
    const checkId = await ArticleCategories.findById(req.params.categoryId);
    if(checkId){
        if(req.user.role == "admin"){
            const removeCategory = await ArticleCategories.findByIdAndRemove(req.params.categoryId);
            /*Remove category from parent category*/ const removeChildCategory = await ArticleCategories.update( {_id : checkId.parentId } ,{$pull : {categoriesChildren : { _id: checkId._id }}})
            if(!removeCategory) {
                return res.status(404).send({
                    message: "Không tìm thấy danh mục này"
                });
            }else{
                if(removeChildCategory){
                    return res.send({
                        message: "Đã xóa thành công danh mục này.",
                        removeChildCategory: removeChildCategory
                    });
                }
                return res.send({
                        message: "Đã xóa thành công danh mục này."
                });
            }
        }
        return res.status(401).send({
            message: "Bạn phải là quản trị viên mới có thể thực hiện tính năng này."
        })
    }else{
        return res.status(404).send({
            message: "Không tìm thấy danh mục này"
        });
    }
};

exports.findCategoryBySlug = async (req, res) =>{
    ArticleCategories.find({slug: req.params.permalink})
    .then(articleCategories => {
        if(!articleCategories) {
            return res.status(404).send({
                message: "Sorry, we can't find this articleCategories."
            });            
        }else{
            res.status(200).send(articleCategories);
        }
    }).catch(err => {
        if(err) {
            return res.status(404).send({
                message: "Sorry, we can't find this articleCategories."
            });                
        }
        return res.status(500).send({
            message: "Error. Please check your internet connection."
        });
    });
}