const mongoose = require('mongoose');
const slug = require('mongoose-slug-generator');
mongoose.plugin(slug);
const Schema = require('mongoose').Schema;

const ArticlesSchema = mongoose.Schema({
    authorId: {
        type: Schema.Types.ObjectId,
        index: true
    },
    authorName: {
        type: String
    },
    title: {
        type: String
    },
    description: {
        type: String
    },
    shortDescription:{
        type: String
    },
    authorAvatar:{
        type: String
    },
    avatar:{
        type: String
    },
    articleCategories:{
        type: Array
    },
    writtingDate:{
        type: Date
    },
    totalLike:{
        type: Number
    },
    totalComment:{
        type: Number
    },
    image:[
        String
    ],
    slug: { type: String, slug: ["title"], unique: true }
}, {
    timestamps: true
});

module.exports = mongoose.model('Article', ArticlesSchema);