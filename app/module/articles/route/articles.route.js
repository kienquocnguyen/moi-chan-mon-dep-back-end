module.exports = router => {
    const articles = require('../controllers/articles.controller.js');
    
    router.post('/articles', Authorization, articles.createArticles);
    router.get('/articles/:permalink', articles.findArticlesBySlug);
    router.get('/articles', articles.findAllArticles);
    router.get('/like/articles', articles.findArticlesByLike);
    router.delete('/delete/articles/:postId', Authorization, articles.deletePost);
}