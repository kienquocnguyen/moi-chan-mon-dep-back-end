const Article = require('../models/articles.model.js');
const Joi = require('@hapi/joi')
.extend(require('@hapi/joi-date'));
const ArticleServices = require('../services/articles.js');
const moment = require('moment');

const schema = Joi.object().keys({
    authorId: Joi.string().optional().messages({
        'string.base': `"authorId" must be a 'string'`,
        'string.empty': `"authorId" cannot be empty`
    }),
    authorName: Joi.string().optional().messages({
        'string.base': `"authorName" must be a 'string'`,
        'string.empty': `"authorName" cannot be empty`
    }),
    title: Joi.string().required().messages({
        'string.base': `"title" must be a 'string'`,
        'string.empty': `"title" cannot be empty`,
        'any.required': `Please enter your "title"`
    }),
    description: Joi.string().required().messages({
        'string.base': `"description" must be a 'string'`,
        'string.empty': `"description" cannot be empty`,
        'any.required': `Please enter your "description"`
    }),
    shortDescription: Joi.string().required().messages({
        'string.base': `"shortDescription" must be a 'string'`,
        'string.empty': `"shortDescription" cannot be empty`,
        'any.required': `Please enter your "shortDescription"`
    }),
    authorAvatar: Joi.string().optional().messages({
        'string.base': `"authorAvatar" need to be upload`,
        'string.empty': `"authorAvatar" need to be upload`
    }),
    avatar: Joi.string().optional().messages({
        'string.base': `"avatar" need to be upload`,
        'string.empty': `"avatar" need to be upload`
    }),
    articleCategories: Joi.array().optional().default([]).messages({
        'array.base': `"articleCategories" must be an array`
    }),
    writtingDate: Joi.date().required().messages({
        'string.base': `"writtingDate" must be a date with format 'dd/mm/yy'`,
        'string.empty': `"writtingDate" cannot be empty`,
        'any.required': `Please enter your "writtingDate"`
    }),
    totalLike: Joi.string().optional().messages({
        'number.base': `"totalLike" must be a 'number'`,
        'number.empty': `"totalLike" cannot be empty`,
        'any.required': `Please enter your "totalLike"`
    }),
    totalComment: Joi.string().optional().messages({
        'number.base': `"totalComment" must be a 'number'`,
        'number.empty': `"totalComment" cannot be empty`,
        'any.required': `Please enter your "totalComment"`
    }),
    image: Joi.array().optional().default([]).messages({
        'array.base': `"image" must be an array`
    }),
}).unknown();

const updateSchema = Joi.object().keys({
    title: Joi.string().required().messages({
        'string.base': `"title" must be a 'string'`,
        'string.empty': `"title" cannot be empty`,
        'any.required': `Please enter your "title"`
    }),
    description: Joi.string().required().messages({
        'string.base': `"description" must be a 'string'`,
        'string.empty': `"description" cannot be empty`,
        'any.required': `Please enter your "description"`
    }),
    shortDescription: Joi.string().required().messages({
        'string.base': `"shortDescription" must be a 'string'`,
        'string.empty': `"shortDescription" cannot be empty`,
        'any.required': `Please enter your "shortDescription"`
    }),
    avatar: Joi.string().optional().messages({
        'string.base': `"avatar" need to be upload`,
        'string.empty': `"avatar" need to be upload`
    }),
    articleCategories: Joi.array().optional().default([]).messages({
        'array.base': `"articleCategories" must be an array`
    }),
    writtingDate: Joi.date().required().messages({
        'string.base': `"writtingDate" must be a date with format 'dd/mm/yy'`,
        'string.empty': `"writtingDate" cannot be empty`,
        'any.required': `Please enter your "writtingDate"`
    }),
    image: Joi.array().optional().default([]).messages({
        'array.base': `"image" must be an array`
    }),
}).unknown();

exports.createArticles = async (req, res, next) =>{
    try {
        const validate = schema.validate(req.body);
        if (validate.error) {
          return next(res.status(400).send(validate.error));
        }

        const data = req.body;
        if(req.user == null){
            return res.status(404).send({
                message: "Your login sessions is out of date"
            });
        }else{
            if(!req.files){
                res.status(400).send({
                    message: "Please upload your avatar"
                });
            }else{
                //Use the name of the input field (i.e. "avatar") to retrieve the uploaded file
                if(req.files.image){
                    let avatar = req.files.avatar;
                    let image = req.files.image;
                    avatar.mv('./uploads/' + avatar.name);
                    data.avatar = avatar.name;
                    data.authorAvatar = req.user.avatar;
                    //Upload libary
                    let imageArray = [];
                    for(let i = 0; i < image.length; i++){
                        image[i].mv('./uploads/' + image[i].name);
                        imageArray.push(image[i].name);
                    }
                    data.image = imageArray;
                }else{
                    let avatar = req.files.avatar;
                    avatar.mv('./uploads/' + avatar.name);
                    data.avatar = avatar.name;
                    data.authorAvatar = req.user.avatar;
                }
                
                //Create A Post
                if(req.user.role == "admin"){
                    data.authorId = req.user._id;
                    data.authorName = req.user.firstName + " " + req.user.lastName;
                    const articles = await ArticleServices.create(data);
                    res.send(articles);   
                }else{
                    res.status(401).send({
                        message: "You must be a 'admin' to do this action"
                    })
                }
            }
        }
        return next();
    }catch(err){
        throw err
    }
}

exports.updateArticles = async (req, res, next) =>{
    try {
        const validate = updateSchema.validate(req.body);
        if (validate.error) {
          return next(res.status(400).send(validate.error));
        }

        const data = req.body;
        if(req.user == null){
            return res.status(404).send({
                message: "Your login sessions is out of date"
            });
        }else{
            if(!req.files){
                res.status(400).send({
                    message: "Please upload your avatar"
                });
            }else{
                //Use the name of the input field (i.e. "avatar") to retrieve the uploaded file
                if(req.files.image){
                    let avatar = req.files.avatar;
                    let image = req.files.image;
                    avatar.mv('./uploads/' + avatar.name);
                    data.avatar = avatar.name;
                    //Upload libary
                    let imageArray = [];
                    for(let i = 0; i < image.length; i++){
                        image[i].mv('./uploads/' + image[i].name);
                        imageArray.push(image[i].name);
                    }
                    data.image = imageArray;
                }else{
                    let avatar = req.files.avatar;
                    avatar.mv('./uploads/' + avatar.name);
                    data.avatar = avatar.name;
                }
                
                //Create A Post
                if(req.user.role == "admin"){
                    Article.findByIdAndUpdate(req.query._id, data, {new: true} )
                    .then(article => {
                        return res.status(200).send(article);
                    }).catch(err => {
                        return res.status(500).send({
                            message: "Lỗi Hệ Thống"
                        });
                    });
                }else{
                    res.status(401).send({
                        message: "You must be a 'admin' to do this action"
                    })
                }
            }
        }
        return next();
    }catch(err){
        throw err
    }
}

exports.findAllArticles = async (req, res, next) =>{
    try {

        let query = Helper.populateDbQuery(req.query, {
            array: [ 'writtingDate', 'title' ],
        });
        const sort = {writtingDate: -1};
        let page = parseInt(req.query.page);
        const limit = parseInt(req.query.limit);
        if(page <= 1){
            page = 0;
        }else{
            page = parseInt(req.query.page) - 1;
        }

        if(req.query.articleCategories){
            query.articleCategories =  {$all: req.query.articleCategories};
        }
        //if startDate has value
        if(req.query.writtingDate){
            Object.assign(query, {startDate: {"$gte": new Date(req.query && req.query.writtingDate ? req.query.writtingDate: '')}});
        }

        const articles = await Article.find(query).skip(page * limit).limit(limit)
        .sort(sort)
        .exec();
        if(articles.length > 0){
            Article.countDocuments()
            .then((docs) =>{
                const total = {
                    "page": page + 1,
                    "limit": limit,
                    "total": docs
                };
                const data = {
                    "articles": articles,
                    "total": total
                }
                return res.send(data)
            })
        }else{
            return res.status(404).send({
             message: "Không thể tìm thấy bài viết trùng khớp với yêu cầu của bạn."
            })
        }
    }catch(err){
        throw err;
    }
}


exports.findArticlesByLike = async (req, res, next) =>{
    try {
        const sort = {totalLike: -1};
        let page = parseInt(req.query.page);
        const limit = parseInt(req.query.limit);
        if(page <= 1){
            page = 0;
        }else{
            page = parseInt(req.query.page) - 1;
        }

        const articles = await Article.find({}).skip(page * limit).limit(limit)
        .sort(sort)
        .exec();
        if(articles.length > 0){
            Article.countDocuments()
            .then((docs) =>{
                const total = {
                    "page": page + 1,
                    "limit": limit,
                    "total": docs
                };
                const data = {
                    "articles": articles,
                    "total": total
                }
                return res.send(data)
            })
        }else{
            return res.status(404).send({
             message: "Sorry, currently we don't have any events match with your searching."
            })
        }
    }catch(err){
        throw err;
    }
}

exports.findArticlesBySlug = async (req, res) =>{
    Article.find({slug: req.params.permalink})
    .then(article => {
        if(!article) {
            return res.status(404).send({
                message: "Sorry, we can't find this article."
            });            
        }else{
            res.status(200).send(article);
        }
    }).catch(err => {
        if(err) {
            return res.status(404).send({
                message: "Sorry, we can't find this article."
            });                
        }
        return res.status(500).send({
            message: "Error. Please check your internet connection."
        });
    });
}

exports.deletePost = async (req, res) => {
    if(req.user == null){
        return res.status(405).send({
            message: "Bạn cần phải đăng nhập để sử dụng được tính năng này."
        })
    }
    const checkId = await Article.findById(req.params.postId);
    if(checkId){
        if(req.user.role == "admin"){
            const removePost = await Article.findByIdAndRemove(req.params.postId);
            if(!removePost){
                return res.status(500).send({message: "Lỗi Hệ Thống"})
            }
            return res.send({
                message: "Đã xóa thành công Bài Viết này."
            });
        }
        return res.status(401).send({
            message: "Bạn phải là quản trị viên mới có thể thực hiện tính năng này."
        })
    }else{
        return res.status(404).send({
            message: "Không tìm thấy bài viết này"
        });
    }
};