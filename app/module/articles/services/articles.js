const Article = require('../models/articles.model.js');

exports.create = async (data) => {
    try {
            const articles = new Article(data);
            await articles.save();
            return articles;
    }catch(e){
        throw e;
    }
}